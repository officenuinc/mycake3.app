<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Core\Exception\Exception;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\TableRegistry;

/**
 * Posts Controller
 *
 * @property \App\Model\Table\PostsTable $Posts
 */
class PostsController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Test');
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->Test->bar(); // Component使用テスト
        $postsTable = TableRegistry::get('Posts');
        $posts = $this->paginate($postsTable);

        $this->set(compact('posts'));
        $this->set('_serialize', ['posts']);
    }

    /**
     * View method
     *
     * @param string|null $id Post id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $post = $this->Posts->get($id, [
            'contain' => []
        ]);

        $this->set('post', $post);
        $this->set('_serialize', ['post']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $postsTable = TableRegistry::get('Posts', ['connection' => ConnectionManager::get('default')]);
        $post = $postsTable->newEntity();
        if ($this->request->is('post')) {
            $connection = ConnectionManager::get('default');
            $connection->begin();
            try {
                $post = $postsTable->patchEntity($post, $this->request->data);
                if ($post = $postsTable->save($post)) {
                    $this->Flash->success(__('保存しました id:' . $post->id));
                    $connection->commit();
                    return $this->redirect(['action' => 'index']);
                } else {
                    throw new Exception(__('保存に失敗しました.'));
                }
            } catch (Exception $e) {
                $connection->rollback();
                $this->Flash->error($e->getMessage());
            }
        }
        $this->set(compact('post'));
        $this->set('_serialize', ['post']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Post id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $postsTable = TableRegistry::get('Posts', ['connection' => ConnectionManager::get('default')]);
        $post = $postsTable->get($id);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $connection = ConnectionManager::get('default');
            $connection->begin();
            try {
                $post = $postsTable->getForUpdate($id);
                $post = $postsTable->patchEntity($post, $this->request->data);
                if ($post = $postsTable->save($post)) {
                    $this->Flash->success(__('保存しました id:' . $post->id));
                    $connection->commit();
                    return $this->redirect(['action' => 'index']);
                } else {
                    throw new Exception(__('保存に失敗しました.'));
                }
            } catch (Exception $e) {
                $connection->rollback();
                $this->Flash->error($e->getMessage());
            }
        }
        $this->set(compact('post'));
        $this->set('_serialize', ['post']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Post id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $postsTable = TableRegistry::get('Posts', ['connection' => ConnectionManager::get('default')]);
        $post = $postsTable->get($id);

        $connection = ConnectionManager::get('default');
        $connection->begin();
        try {
            if ($post = $postsTable->delete($post)) {
                $this->Flash->success(__('削除しました'));
                $connection->commit();
            } else {
                throw new Exception(__('削除に失敗しました.'));
            }
        } catch (Exception $e) {
            $connection->rollback();
            $this->Flash->error($e->getMessage());
        }
        return $this->redirect(['action' => 'index']);
    }
}
