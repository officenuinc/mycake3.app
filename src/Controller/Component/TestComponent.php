<?php

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Log\Log;

class TestComponent extends AppComponent
{
	// The other component your component uses
	public $components = ['Test2'];

	// Execute any other additional setup for your component.
	public function initialize(array $config)
	{
		$this->Test2->foo();
	}

	public function bar()
	{
		// ...
		Log::write('debug', "TestComponent bar");
	}
}