<?php

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Log\Log;

class Test2Component extends AppComponent
{
	// Execute any other additional setup for your component.
	public function initialize(array $config)
	{
	}

	public function foo()
	{
		// ...
		Log::write('debug', "Test2Component foo");
	}
}