<?php
namespace App\Model\Table;

use App\Model\Entity\Post;
use App\Model\Table\AppTable;
use Cake\Validation\Validator;

/**
 * Posts Model
 *
 */
class PostsTable extends AppTable
{
    /**
     * @return string
     */
    public static function defaultConnectionName() {
        return 'slave';
    }

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('posts');
        $this->displayField('title');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('title');

        $validator
            ->allowEmpty('body');

        return $validator;
    }

    /**
     * @param $id
     * @param $contain
     * @return mixed
     */
    public function getForUpdate($id, $contain = [])
    {
        return $this->find()
            ->epilog('FOR UPDATE')
            ->contain($contain)
            ->where(['id' => $id])
            ->first();
    }
}
